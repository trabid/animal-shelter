import React, {Component} from 'react';
import {Route, NavLink, Switch} from "react-router-dom";
import {Container} from 'reactstrap';
import './App.css';
import Header from "./component/Header";
import Home from "./component/Home"
import Catalog from "./component/Catalog"
import Contact from "./component/Contact"
import Footer from "./component/footer";
import Login from "./component/Login";
import Manager from "./component/AnimalManager";

class App extends React.Component {

    render() {
        return (
            <div>
                <Header/>
                <div class="container app-body">
                    <Container fluid>
                        <div
                            className="row header justify-content-md-center">{/*<FirsBox /><SecondBox /><Money/>*/}
                        </div>
                        <Switch>
                            <Route exact path="/" component={Home}/>
                            <Route exact path="/catalog" component={Catalog}/>
                            <Route exact path="/contact" component={Contact}/>
                            <Route exact path="/login" component={Login}/>
                            <Route exact path="/manager" component={Manager}/>
                        </Switch>
                    </Container>
                </div>
                <Footer className="footer" />
            </div>
        );
    }
}

export default App;
