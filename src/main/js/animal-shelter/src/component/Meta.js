import React from 'react';
import MetaTags from 'react-meta-tags';

class Meta extends React.Component {
    render() {
        return (
            <div className="wrapper">
                <MetaTags>
                    <title>Page 1</title>
                    <meta property="og:title" content="Animal shelter"/>
                    <meta property="og:image" content="path/to/image.jpg"/>
                    <meta name="keywords" content="schronisko, psy, koty, adopcja, zwierze, zwierzęta"/>
                    <meta name="description" content="Schronisko dla zwierząt. Znajdź pupila, który umili Ci codzienne życie."/>
                    <meta name="subject" content="Schronisko dla zwierząt"/>
                    <meta name="copyright" content="Damian Gręda sky event"/>
                    <meta name="language" content="PL"/>
                    <meta name="robots" content="index"/>
                    <meta name="revised" content="Sunday, January 6th, 2019, 5:15 pm"/>
                    <meta name="Classification" content="Business"/>
                    <meta name="author" content="name, damiangreda93@gmail.com"/>
                    <meta name="designer" content="Damian Gręda"/>
                    <meta name="copyright" content="sky event"/>
                    <meta name="owner" content="Damian Gręda"/>
                    <meta name="category" content="zwierzęta"/>
                    <meta name="coverage" content="Worldwide"/>
                    <meta name="distribution" content="Global"/>
                    <meta name="rating" content="General"/>
                    <meta name="revisit-after" content="7 days"/>
                    <meta http-equiv="Expires" content="0"/>
                    <meta http-equiv="Pragma" content="no-cache"/>
                    <meta http-equiv="Cache-Control" content="no-cache"/>
                </MetaTags>
                <div className="content"> Some Content</div>
            </div>
        )
    }
}