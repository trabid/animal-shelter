import React, { Component } from "react";
import {Link} from "react-router-dom";
import {Button, Container, Card, CardBody, CardTitle, CardSubtitle, CardImg} from 'reactstrap';
import '../style/catalog.css';
import logo from '../img/brand/dogo.png'
// import image from '../public/dogs/1.jpg'
import Footer from './footer'
class Catalog extends Component {
    constructor(props) {
        super(props);
        this.state = {
            animals: [],
            isLoading: true,
            search: null,
        };
        // this.pageSize = 20;
        // this.pagesCount = Math.ceil(Object.keys(this.props.animals).length/this.pageSize)
        // this.remove=this.remove.bind(this);
    }

    componentDidMount() {
        const search = this.props.search;
        // console.log('propsss ' + search);
        this.setState({isLoading: true});
        if (search !== '') {

            var url = new URL('http://localhost:3000/animals');
            fetch(url)
                .then(response => response.json())
                .then(data => this.setState({animals: data, isLoading: false}))
            // .then((()=>this.setState({pageCount:Math.ceil(Object.keys(this.state.animals).length/this.pageSize)})));
        } else {
            fetch(url)
                .then(response => response.json())
                .then(data => this.setState({animals: data, isLoading: false}))
            // .then((()=>this.setState({pageCount:Math.ceil(Object.keys(this.state.animals).length/this.pageSize)})));
        }
        // var ar = [];
        // ar = this.state.animals;
        // this.setState({pageCount: Math.ceil(Object.keys(ar).length / this.pageSize)});
    }

    handleClick(e, index) {
        e.preventDefault()
        this.setState({
            currentPage: index
        });
    }

    render() {
        const {animals, isLoading} = this.state;

        if (isLoading) {
            return (<div>
                    <h1>
                        <p>Loading...</p>
                    </h1>
                </div>
            );
        }

        const resultList = animals.map( result => {
            return <div class="result" key={result.id}>
                <Card>
                    <div class="imgg">
                        <CardImg top width="50px" high="100px" src={"dogs/"+result.image} alt="animal image"/>
                    </div>
                    <CardBody>
                        <CardTitle>{result.price.toFixed(2)} PLN</CardTitle>
                        <div class="subtitle">
                            <CardSubtitle>{result.race}</CardSubtitle>
                            <p>{"wiek: "+result.age}</p>
                            <p>{"waga: "+result.weight}</p>
                        </div>
                        <div class="btn-div">
                            <a  href={"contact"} >
                                <Button size="lg" color="primary" block>{result.name}</Button>
                            </a>
                        </div>
                    </CardBody>
                </Card>
            </div>
        });

        return (
            <div className="login">
                <Container>
                    <img className="navbar-brand-full" src={logo} width="300" height="300" alt="dog Logo"/>
                    <h1>Lista dostępnych zwierząt {Object.keys(animals).length} </h1>
                    <div className="app-body" class="container">
                        {resultList}
                    </div>
                </Container>
                <h3>koniec</h3>
                <p className="paragraph">
                </p>
            {/*<Footer/>*/}
            </div>
        );
    }
}

export default Catalog;