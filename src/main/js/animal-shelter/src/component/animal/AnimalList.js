import React from 'react';
import { Table } from 'reactstrap';
import Animal from './Animal'
class AnimalList extends React.Component{
	render() {
		var i = 1;
		const animals = this.props.animals.map(animal =>
			<Animal key={animal.id} animal={animal} count={i++}/>
		);
		return (
			<Table responsive striped hover className="table table-striped">
					<thead>
					<tr>
						<th>#</th>
						<th>Imię</th>
						<th>Rasa</th>
						<th>Wiek</th>
						<th>Waga</th>
						<th>Cena</th>
						<th>Zdjęcie</th>
					</tr>
					</thead>
				<tbody>
					{animals}
				</tbody>
			</Table>
		)
	}
}
export default AnimalList;