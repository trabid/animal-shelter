import React from 'react';
import {Button} from 'reactstrap';

class Animal extends React.Component {
    constructor(props) {
        super(props);
        this.handleDelete = this.handleDelete.bind(this);
    }

    handleDelete(id, e) {
        e.preventDefault();
        fetch('http://localhost:3000/animals/delete/' + id, {
            method: 'DELETE'
        });

        var millisecondsToWait = 700;
        setTimeout(function () {
            window.location.reload();
        }, millisecondsToWait);
        // var self = this;
        // $.ajax({
        //     url: self.props.employee._links.self.href,
        //     type: 'DELETE',
        //     success: function(result) {
        //       self.setState({display: false});
        //     },
        //     error: function(xhr, ajaxOptions, thrownError) {
        //       toastr.error(xhr.responseJSON.message);
        //     }
        // });
    }

    render() {
        return (
            <tr>
                <th scope="row">{this.props.count}</th>
                <td>{this.props.animal.name}</td>
                <td>{this.props.animal.race}</td>
                <td>{this.props.animal.age}</td>
                <td>{this.props.animal.weight}</td>
                <td>{this.props.animal.price}</td>
                <td><img src={"dogs/" + this.props.animal.image} className="thumbnail" width="50px" height="50px"/></td>
                <td><Button color="danger" onClick={e => this.handleDelete(this.props.animal.id, e)}>Oddaj do
                    adpocji</Button></td>
            </tr>
        )
    }
}

export default Animal;