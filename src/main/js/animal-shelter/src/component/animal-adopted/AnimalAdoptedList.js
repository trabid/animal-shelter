import React from 'react';
import { Table } from 'reactstrap';
import AnimalAdopted from './AnimalAdopted'
class AnimalAdoptedList extends React.Component{
    render() {
        var i = 1;
        const animalsAdopted = this.props.animalsAdopted.map(animal =>
            <AnimalAdopted key={animal.id} animal={animal} count={i++}/>
        );
        return (
            <Table responsive striped hover className="table table-striped">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Imię</th>
                    <th>Rasa</th>
                    <th>Wiek</th>
                    <th>Waga</th>
                    <th>Cena</th>
                    <th>Data adopcji</th>
                    <th>Pracownik</th>
                    <th>Zdjęcie</th>
                </tr>
                </thead>
                <tbody>
                {animalsAdopted}
                </tbody>
            </Table>
        )
    }
}
export default AnimalAdoptedList;