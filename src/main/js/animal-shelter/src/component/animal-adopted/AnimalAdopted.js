import React from 'react';
import {Button} from 'reactstrap';

class AnimalAdopted extends React.Component{
    constructor(props){
        super(props);
    }

    render() {
        return (
            <tr>
                <th scope="row">{this.props.count}</th>
                <td>{this.props.animal.name}</td>
                <td>{this.props.animal.race}</td>
                <td>{this.props.animal.age}</td>
                <td>{this.props.animal.weight}</td>
                <td>{this.props.animal.price}</td>
                <td>{this.props.animal.adoptionDate.toString()}</td>
                <td>{this.props.animal.adopterName}</td>
                <td><img src={"dogs/"+this.props.animal.image} className="thumbnail" width="50px" height="50px" /></td>
            </tr>
        )
    }
}
export default AnimalAdopted;