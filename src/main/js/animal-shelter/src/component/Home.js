import React, { Component } from "react";
import {Link} from 'react-router-dom';
import Carousel from './carousel';
import { Jumbotron, Button } from 'reactstrap';
import info from '../img/info.png'


class Home extends Component {
    render() {
        return (
            <div>
                <div id="maska">
                <h1 className="text-center">Czekam na Ciebie !</h1>
                </div>
                <Carousel/>
                <Jumbotron>
                    <h1 className="display-3">Zaadoptuj pupila !</h1>
                    <p className="lead">Psy, koty, czasem króliki, chomiki czy szczury – zagubione, porzucone czy odebrane
                        interwencyjnie właścielom – znajdują u nas swój tymczasowy dom. I czekają na nowy, lepszy.
                        Zapewniamy im opiekę i pomoc weterynaryjną</p>
                    <hr className="my-2" />
                    <p>Co roku do schroniska trafia ok. półtora tysiąca zwierząt. Więcej jest kotów niż psów.
                        Sporo z nich (np. połowa psów) jest odbierana przez swoich właścicieli. Reszta,
                        po okresie kwarantanny, czeka na dom. Co roku mamy 700-800 adopcji.</p>
                    <p className="lead">
                        <Button color="primary" tag={Link} to={"/catalog"}>Zaadoptuj</Button>
                    </p>
                </Jumbotron>
                <img class ="center" src={info} alt ="infografika" />
            </div>
        );
    }
}
export default Home;