import React from 'react';
import {Link} from 'react-router-dom';
import logo from "../img/brand/love.png";
import sygnet from "../img/brand/dog.png";
import {
    Collapse, Nav, Navbar, NavbarBrand, NavbarToggler, NavItem, NavLink,
    DropdownItem, Dropdown, DropdownMenu, DropdownToggle
} from 'reactstrap';
import '../style/header.css'

class Footer extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isOpen: false,
            meIsOpen: false
        };
        this.toggle = this.toggle.bind(this);
        this.meToggle = this.meToggle.bind(this);
    }

    toggle() {
        this.setState({isOpen: !this.state.isOpen});
    }

    meToggle() {
        this.setState({meIsOpen: !this.state.meIsOpen});
    }


    render() {
        return (
            <Navbar id="topnav" color="dark" dark expand="md" fluid="true">
                <NavbarBrand>&copy;2019 Schronisko dla zwierząt</NavbarBrand>
                <NavbarToggler onClick={this.toggle}/>
                <Collapse isOpen={this.state.isOpen} navbar>
                    <Nav className="ml-auto" navbar>
                        <NavItem>
                            <NavLink className="nav-link-dark" tag={Link} to="/login">Logowanie</NavLink>
                        </NavItem>
                        {/*<NavItem>*/}
                            {/*<NavLink className="nav-link-dark" tag={Link} to="/manager">Panel administracyjny</NavLink>*/}
                        {/*</NavItem>*/}
                        <Dropdown isOpen={this.state.meIsOpen} toggle={this.meToggle}>
                            <DropdownToggle color="dark" className="dropdown-toggle">
                                O mnie
                            </DropdownToggle>
                            <DropdownMenu class="dropdown-bggg" className="bg-dark">
                                <NavItem>
                                    <NavLink
                                        href="https://pl.linkedin.com/in/damian-gr%C4%99da-485689147">@LinkedIn</NavLink>
                                </NavItem>
                                <DropdownItem divider/>
                                <NavItem>
                                    <NavLink href="https://bitbucket.org/trabid/searcher.git">@Bitbucket</NavLink>
                                </NavItem>
                            </DropdownMenu>
                        </Dropdown>
                    </Nav>
                </Collapse>
            </Navbar>
        );
    }
}

export default Footer