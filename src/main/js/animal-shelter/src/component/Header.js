import React from 'react';
import {Link} from 'react-router-dom';
import logo from "../img/brand/logo-dog.svg";
import {
    Collapse, Nav, Navbar, NavbarBrand, NavbarToggler, NavItem, NavLink,
    DropdownItem, Dropdown, DropdownMenu, DropdownToggle
} from 'reactstrap';
import '../style/header.css'

class Header extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isOpen: false,
            meIsOpen: false
        };
        this.toggle = this.toggle.bind(this);
        this.meToggle = this.meToggle.bind(this);
    }

    toggle() {
        this.setState({isOpen: !this.state.isOpen});
    }

    meToggle() {
        this.setState({meIsOpen: !this.state.meIsOpen});
    }


    render() {
        return (
            <Navbar id="topnav" color="dark" dark expand="md" fluid="true">
                <NavbarBrand tag={Link} to="/">
                    <img className="navbar-brand-full App-logo" src={logo} width="89" height="50" alt="dog logo"/>
                </NavbarBrand>
                {/*<NavbarToggler onClick={this.toggle}/>*/}
                {/*<Collapse isOpen={this.state.isOpen} navbar>*/}
                        <NavItem>
                            <NavLink className="nav-link-dark" tag={Link} to="/">Strona główna</NavLink>
                        </NavItem>
                    <Nav className="ml-auto" navbar>
                        <NavItem>
                            <NavLink className="nav-link-dark" tag={Link} to="/catalog">Katalog zwierząt</NavLink>
                        </NavItem>
                        <NavItem>
                            <NavLink className="nav-link-dark" tag={Link} to="/contact">Kontakt</NavLink>
                        </NavItem>
                        <Dropdown isOpen={this.state.meIsOpen} toggle={this.meToggle}>
                            <DropdownToggle  color="dark" className="dropdown-toggle" >
                                O mnie
                            </DropdownToggle>
                            <DropdownMenu class="dropdown-bggg" className="bg-dark">
                                    <NavItem >
                                        <NavLink
                                            href="https://pl.linkedin.com/in/damian-gr%C4%99da-485689147">@LinkedIn</NavLink>
                                    </NavItem>
                                <DropdownItem divider/>
                                    <NavItem>
                                        <NavLink href="https://bitbucket.org/trabid/searcher.git">@Bitbucket</NavLink>
                                    </NavItem>
                            </DropdownMenu>
                        </Dropdown>
                    </Nav>
                {/*</Collapse>*/}
            </Navbar>
        );
    }

// render(){
//     return(
//         <header style={{ top: 0 }} className="app-header navbar">
//             <button className="navbar-toggler sidebar-toggler d-lg-none mr-auto" type="button" data-toggle="sidebar-show">
//
//             </button>
//             <a className="navbar-brand" href="#">
//                 <img className="navbar-brand-full" src={logo} width="89" height="25" alt="CoreUI Logo" />
//                 <img className="navbar-brand-minimized" src={sygnet} width="30" height="30" alt="CoreUI Logo" />
//             </a>
//             <button className="navbar-toggler sidebar-toggler d-md-down-none" type="button" data-toggle="sidebar-lg-show">
//             </button>
//             <ul className="nav navbar-nav d-md-down-none">
//                 <li className="nav-item px-3">
//                     <Link to="/city">Miasta</Link>
//                 </li>
//                 <li className="nav-item px-3">
//                     <Link to="/holidays">Swieta</Link>
//                 </li>
//             </ul>
//         </header>
//     )
// }
}

export default Header