import React, { Component } from "react";
import {Form , FormGroup, FormText, Input, Label,Button, } from 'reactstrap';
import mail from '../img/mail.png'

class Contact extends Component {
    constructor(props){
        super(props);
        this.handleSendMail = this.handleSendMail.bind(this);
        this.state = {
            sender:"",
            subject:"",
            message:""
        }
    }

    handleSendMail(e){
        e.preventDefault();
        let sender = this.state.sender;
        let subject = this.state.subject;
        let message = this.state.message;

        var url = 'http://localhost:3000/mail/save';
        fetch(url, {
            method:'POST',
            headers: {
                'Accept': 'application/json, text/plain, */*',
                'Content-type':'application/json'
            },
            body:
                JSON.stringify({
                    "sender":sender,
                    "subject":subject,
                    "message":message
                })

        })
            .then(resp => resp.json())
            .then(data => console.log(data));
        var millisecondsToWait = 700;
        setTimeout(function() {
            window.location.reload();
        }, millisecondsToWait);

    }
    render() {
        return (
            <div className="center login">
                <img src={mail} className="center" />
                <h1>Wyślij nam swoją wiadomość</h1>
                <Form >
                    <FormGroup>
                        {/*<Label for = "email"> Twój e-mail:</Label>*/}
                        <Input type = "email" className="center login-input" id = "email" placeholder="Twój email"
                        value = {this.state.sender} onChange = {(ev)=>this.setState({sender:ev.target.value})}/>
                    </FormGroup>
                    <FormGroup>
                        {/*<Label for="subject">Temat:</Label>*/}
                        <Input type="text" className="center login-input" id = "subject" placeholder = "Temat"
                               value = {this.state.subject} onChange = {(ev)=>this.setState({subject:ev.target.value})}/>
                    </FormGroup>
                    <FormGroup>
                        {/*<Label for = "msg">Treść wiadomości:</Label>*/}
                        <Input type="textarea" className="center login-msg" id="msg" placeholder="Twoja wiadomość"
                               value = {this.state.message} onChange = {(ev)=>this.setState({message:ev.target.value})}/>
                    </FormGroup>
                    <FormGroup>
                        {/*<Label for = "file">Twój plik</Label>*/}
                        <Input type ="file" className="center login-input" id = "fileAttach" />
                    </FormGroup>
                    <Button onClick={this.handleSendMail} color="primary">Wyślij</Button>
                </Form>
            </div>
        );
    }
}

export default Contact;