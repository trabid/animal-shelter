import React from 'react';
import {TabContent, TabPane, Nav, NavItem, NavLink, Button,
    Form, FormGroup,Label, Input , Row, Col, ButtonGroup} from 'reactstrap';
import classnames from 'classnames';
import AnimalList from './animal/AnimalList';
import AnimalAdoptedList from './animal-adopted/AnimalAdoptedList';

export default class AnimalManager extends React.Component {
    constructor(props) {
        super(props);
        this.handleAddAnimal = this.handleAddAnimal.bind(this);
        this.handlePDF = this.handlePDF.bind(this);
        this.toggle = this.toggle.bind(this);
        this.state = {
            name:"",
            race:"",
            price:0.00,
            img:"",
            weight:0,
            age:0,
            activeTab: '1',
            animals: [],
            animalsAdopted: [],
            isLoading: true
        };
    }

    handlePDF(e){
        e.preventDefault();
        var url = new URL('http://localhost:3000/file/pdf');
        fetch(url)
            .then(response => response.json())
            .then(data => this.setState({animals: data, isLoading: false}))
    }

    handleAddAnimal = (e) => {
        e.preventDefault();

        let name = this.state.name;
        let race = this.state.race;
        let price = this.state.price;
        let weight = this.state.weight;
        let age = this.state.age;
        let img= this.state.img;
        var url = 'http://localhost:3000/animals/save';

        fetch(url, {
            method:'POST',
            headers: {
                'Accept': 'application/json, text/plain, */*',
                'Content-type':'application/json'
            },
            body:
                // "name":name,
                // "race":race,
                // "price":price,
                // "weight":weight,
                // "img":img
                JSON.stringify({
                "name":name,
                "race":race,
                "price":price,
                "weight":weight,
                "age":age,
                "image":img
                })

        })
            .then(resp => resp.json())
            .then(data => console.log(data));
        var millisecondsToWait = 700;
        setTimeout(function() {
        window.location.reload();
        }, millisecondsToWait);

    }

    componentDidMount() {
        // client({method: 'GET', path: '/api/employees'}).done(response => {
        // 	this.setState({animals: response.entity._embedded.animals});
        // });
        // console.log('propsss ' + search);
        this.setState({isLoading: true});
        var url = new URL('http://localhost:3000/animals');
        fetch(url)
            .then(response => response.json())
            .then(data => this.setState({animals: data, isLoading: false}))
        // .then((()=>this.setState({pageCount:Math.ceil(Object.keys(this.state.animals).length/this.pageSize)})));

    }

    toggle(tab) {
        if (this.state.activeTab !== tab) {
            this.setState({
                activeTab: tab
            });
        }
        if (tab === "3") {
            var url = new URL('http://localhost:3000/adopted');
            fetch(url)
                .then(response => response.json())
                .then(data => this.setState({animalsAdopted: data}))
        }
    }

    render() {
        const isLoading = this.state;
        // const loader ={if (isLoading) {
        //     return (<div>
        //             <h1>
        //                 <p>Loading...</p>
        //             </h1>
        //         </div>
        //     );
        // }};
        return (
            <div>
                <Nav tabs>
                    <NavItem>
                        <NavLink
                            className={classnames({active: this.state.activeTab === '1'})}
                            onClick={() => {
                                this.toggle('1');
                            }}>
                            Lista zwierząt
                            {/*{loader}*/}
                        </NavLink>
                    </NavItem>
                    <NavItem>
                        <NavLink
                            className={classnames({active: this.state.activeTab === '2'})}
                            onClick={() => {
                                this.toggle('2');
                            }}
                        >
                            Dodaj zwierzę
                        </NavLink>
                    </NavItem>
                    <NavItem>
                        <NavLink
                            className={classnames({active: this.state.activeTab === '3'})}
                            onClick={() => {
                                this.toggle('3');
                            }}>
                            Lista zwierząt adoptowanych
                        </NavLink>
                    </NavItem>
                </Nav>
                <TabContent activeTab={this.state.activeTab}>
                    <TabPane tabId="1">
                        <Row>
                            <Col sm="12">
                                <h4>Lista zwierząt w bazie danych</h4>
                                <ButtonGroup>

                                <a href="http://localhost:8080/file/pdf">
                                <Button >PDF</Button>
                                </a>

                                <a href="http://localhost:8080/file/csv">
                                    <Button >CSV</Button>
                                </a>
                                </ButtonGroup>
                                <AnimalList animals={this.state.animals}/>
                            </Col>
                        </Row>
                    </TabPane>
                    <TabPane tabId="2">
                        <Row>
                            <div>
                                <h2>Dodaj zwierzę</h2>
                                <Form>
                                    <FormGroup>
                                        <Label for="name">Imię</Label>
                                        <Input type="text" id="name" placeholder="Imię" value={this.state.name} onChange = {(ev)=>this.setState({name:ev.target.value})}/>
                                    </FormGroup>
                                    <FormGroup>
                                        <Label for="race">Rasa</Label>
                                        <Input type="text" id="race" placeholder="Rasa" value={this.state.race} onChange = {(ev)=>this.setState({race:ev.target.value})}/>
                                    </FormGroup>
                                    <FormGroup>
                                        <Label for="price">Cena</Label>
                                        <Input type="text" id="price" placeholder="Cena" value={this.state.price} onChange = {(ev)=>this.setState({price:parseInt(ev.target.value)})}/>
                                    </FormGroup>
                                    <FormGroup>
                                        <Label for="weight">Waga</Label>
                                        <Input type="text" id="weight" placeholder="Weight" value={this.state.weight} onChange = {(ev)=>this.setState({weight:parseInt(ev.target.value)})}/>
                                    </FormGroup>
                                    <FormGroup>
                                        <Label for="weight">Wiek</Label>
                                        <Input type="text" id="weight" placeholder="Weight" value={this.state.age} onChange = {(ev)=>this.setState({age:parseInt(ev.target.value)})}/>
                                    </FormGroup>
                                    <FormGroup>
                                        <Label for="image">Zdjęcie</Label>
                                        <Input type="text" id="img" placeholder="img" value={this.state.img} onChange = {(ev)=>this.setState({img:ev.target.value})}/>
                                        <Input type="file" id="image"/>
                                    </FormGroup>
                                    <Button color="primary" onClick={this.handleAddAnimal}>Dodaj</Button>
                                </Form>
                            </div>
                        </Row>
                    </TabPane>
                    <TabPane tabId="3">
                        <Row>
                            <Col sm="12">
                                <h4>Lista adoptowanych zwierząt w bazie danych</h4>
                                <AnimalAdoptedList animalsAdopted={this.state.animalsAdopted}/>
                            </Col>
                        </Row>
                    </TabPane>
                </TabContent>
            </div>
        );
    }
}