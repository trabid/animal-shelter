import React from 'react';
import {Button, Form, FormGroup,Label, Input } from 'reactstrap';
import {Link} from 'react-router-dom';

class Login extends React.Component{
    constructor(props){
        super(props);
        this.handleClick = this.handleClick.bind(this);
        {/*<Link/>*/}
    }

handleClick(event){
    console.log("sss");

     }

	 render() {
		return (
			<div className="formularz login" >
                <Form>
                    <h2>Zaloguj się do panelu administracyjnego</h2>
                    <FormGroup>
                        {/*<Label for="email">Adres e-mail</Label>*/}
                        <Input type="email" className="login-input center" id="email" placeholder="E-mail" />
                    </FormGroup>
                    <FormGroup>
                        {/*<Label for="password">Hasło</Label>*/}
                        <Input type="password" className="login-input center" id="password" placeholder="Hasło" />
                    </FormGroup>
                    <Button color="primary" tag={Link} to="manager"  onClick={this.handleClick}>Zaloguj</Button>
                </Form>
            </div>
		)
	}
}

export default Login;
   // @Override
   //      protected void configure(HttpSecurity http) throws Exception {
   //          http.authorizeRequests()
   //              .antMatchers("/hello/**").hasRole("ADMIN")
   //              .anyRequest().permitAll()
   //              .and()
   //                  .formLogin().loginPage("/")
   //                      .usernameParameter("username").passwordParameter("password")
   //                      .defaultSuccessUrl("hello", true)
   //                      .and()
   //                      .httpBasic()
   //              .and()
   //                  .csrf().disable();
   //          }
   //
   //      @Autowired
   //      public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
   //                 auth
   //                  .inMemoryAuthentication()
   //                    .withUser("user").password("user").roles("USER")
   //           .and()
   //                   .withUser("admin").password("admin").roles("ADMIN");
   //              }