package com.dg.animalshelter.web.security;

import com.dg.animalshelter.database.service.EmployeeService;
import com.dg.animalshelter.database.model.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
public class EmployeeUserDetailsService implements UserDetailsService {
    @Autowired
    private EmployeeService employeeService;

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        Employee employee = employeeService.getByEmail(email);
        if(employee == null) {
            throw new UsernameNotFoundException(email);
        }
            return new EmployeePrinciple(employee);
    }
}
