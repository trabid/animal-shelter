package com.dg.animalshelter.web.controller;

import com.dg.animalshelter.database.model.MailEntity;
import com.dg.animalshelter.database.service.MailService;
import com.dg.animalshelter.domain.mail.SmtpMailProvider;
import com.dg.animalshelter.domain.mail.Mail;
import com.dg.animalshelter.domain.mail.MailSender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@RestController
@RequestMapping("mail")
public class MailRestController {

    private MailService mailService;

    @Autowired
    public MailRestController(MailService mailService) {
        this.mailService = mailService;
    }

    @GetMapping
    public ResponseEntity<Collection<MailEntity>> getAll() {
            return ResponseEntity.ok().body(mailService.getAll());
    }

    @GetMapping("/{id}")
    public ResponseEntity<MailEntity> getMailById(@PathVariable long id) {
            return ResponseEntity.ok().body(mailService.getById(id));
    }

    @PostMapping(value = "/save",consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<MailEntity> saveMail(@RequestBody Mail mail) {
        MailEntity mailEntity = mail.toDto();
        if (mail != null) {
            mailService.save(mailEntity);
            MailSender mailSender = new MailSender(new SmtpMailProvider());
//            mailSender.sendMail(mail);
            return ResponseEntity.status(201).build();
        }
        return ResponseEntity.unprocessableEntity().body(mailEntity);
    }
}
