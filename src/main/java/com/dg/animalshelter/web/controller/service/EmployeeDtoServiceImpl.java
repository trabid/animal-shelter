package com.dg.animalshelter.web.controller.service;

import com.dg.animalshelter.database.service.EmployeeService;
import com.dg.animalshelter.web.controller.dto.EmployeeDto;
import com.dg.animalshelter.database.model.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class EmployeeDtoServiceImpl implements EmployeeDtoService {

    @Autowired
    private EmployeeService employeeService;

    @Override
    public void save(EmployeeDto employeee) {
    }

    @Override
    public void update(EmployeeDto employee) {

    }

    @Override
    public void delete(Long id) {

    }

    @Override
    public Collection<EmployeeDto> getAll() {
        return null;
    }

    @Override
    public Employee getByEmail(String email) {
        return null;
    }
}
