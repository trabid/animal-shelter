package com.dg.animalshelter.web.controller;

import com.dg.animalshelter.database.model.AnimalAdopted;
import com.dg.animalshelter.database.service.AnimalAdoptedService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;

@RestController
@RequestMapping("adopted")
public class AnimalAdoptedRestController {

    @Autowired
    private AnimalAdoptedService animalAdoptedService;

    @GetMapping
    public Collection<AnimalAdopted> getAnimals() {
        return animalAdoptedService.getAll();
    }
}
