package com.dg.animalshelter.web.controller.service;

import com.dg.animalshelter.web.controller.dto.AnimalDto;
import com.dg.animalshelter.database.model.Animal;
import com.dg.animalshelter.database.service.AnimalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.stream.Collectors;

@Service
public class AnimalDtoServiceImpl implements AnimalDtoService {

    @Autowired
    private AnimalService animalService;

    @Override
    public Collection<AnimalDto> getAll() {
        return animalService.getAll().parallelStream().map(g -> new AnimalDto(g)).collect(Collectors.toList());
    }

    @Override
    public AnimalDto getById(long id) {
        return new AnimalDto(animalService.getById(id));
    }

    @Override
    public AnimalDto save(AnimalDto animal) {
        return new AnimalDto( animalService.save(new Animal.Builder(animal.getName(),animal.getRace())
                .setAge(animal.getAge())
        .setImage(animal.getImage())
        .setPrice(animal.getPrice())
        .setWeight(animal.getWeight())
        .build()));
    }

    @Override
    public boolean existsById(long id) {
        return animalService.existsById(id);
    }

    @Override
    public boolean isFull() {
        return animalService.isFull();
    }

    @Override
    public void deleteById(long id) {
        animalService.deleteById(id);
    }

    @Override
    public AnimalDto update(AnimalDto animal) {
        return new AnimalDto(animalService.save(new Animal.Builder(animal.getName(),animal.getRace())
                .setAge(animal.getAge())
                .setId(animal.getId())
                .setImage(animal.getImage())
                .setPrice(animal.getPrice())
                .setWeight(animal.getWeight())
                .build()));
    }
}
