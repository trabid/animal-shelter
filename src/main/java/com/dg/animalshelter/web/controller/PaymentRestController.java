package com.dg.animalshelter.web.controller;

import com.dg.animalshelter.database.model.Payment;
import com.dg.animalshelter.database.service.PaymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("payment")
public class PaymentRestController {

    @Autowired
    private PaymentService paymentService;

    @PostMapping
    public ResponseEntity<Payment> pay(@RequestBody Payment payment) {
        paymentService.save(payment);
        return ResponseEntity.ok().build();
    }
}
