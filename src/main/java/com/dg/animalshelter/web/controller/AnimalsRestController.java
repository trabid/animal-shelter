package com.dg.animalshelter.web.controller;

import com.dg.animalshelter.database.model.Animal;
import com.dg.animalshelter.database.service.AnimalAdoptedService;
import com.dg.animalshelter.domain.exception.ResourceNotFoundException;
import com.dg.animalshelter.web.controller.dto.AnimalDto;
import com.dg.animalshelter.web.controller.service.AnimalDtoService;
import com.dg.animalshelter.database.model.AnimalAdopted;
import com.dg.animalshelter.database.model.Capacity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;

@RestController
@RequestMapping("animals")
public class AnimalsRestController {

    @Autowired
    private AnimalDtoService animalDtoService;
    @Autowired
    private AnimalAdoptedService animalAdoptedService;

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public Collection<AnimalDto> getAnimals(HttpServletResponse response) {
        ArrayList<AnimalDto> results = new ArrayList<>(animalDtoService.getAll());
        response.addIntHeader("available-boxes", Capacity.SMALL.get() - results.size());
        return results;
    }

    @GetMapping(value = "/get/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<AnimalDto> getAnimal(@PathVariable long id) {
        if (animalDtoService.existsById(id)) {
            return ResponseEntity.ok().body(animalDtoService.getById(id));
        } else {
            throw new ResourceNotFoundException("Sorry! AnimalAdopted with provided id not found");
        }
    }

    @PostMapping(value = "/save", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity createAnimal(@RequestBody AnimalDto animal) {
        if (animal != null) {
            try {
                animalDtoService.save(animal);
                return ResponseEntity.status(201).build();
            } catch (IndexOutOfBoundsException e) {
                return ResponseEntity.status(507).header("msg", "Shelter is full").build();
            }
        }
        return ResponseEntity.unprocessableEntity().body(animal);
    }

    @PutMapping("/{id}")
    public ResponseEntity<AnimalDto> updateAnimal(@PathVariable long id, @RequestBody Animal animal) {
        AnimalDto currentAnimal = animalDtoService.getById(id);

        if (currentAnimal != null) {
            currentAnimal = new AnimalDto.Builder(animal.getName(), animal.getRace())
                    .setAge(animal.getAge())
                    .setId(id)
                    .setImage(animal.getImage())
                    .setPrice(animal.getPrice())
                    .setWeight(animal.getWeight())
                    .build();
            animalDtoService.update(currentAnimal);
            ResponseEntity.noContent().build();
        }
        return ResponseEntity.unprocessableEntity().build();
    }

    @DeleteMapping(value = "/delete/{id}")
    public ResponseEntity<AnimalDto> deleteOne(@PathVariable long id) {
        AnimalDto animal = animalDtoService.getById(id);
        AnimalAdopted animalAdopted = new AnimalAdopted.Builder(animal)
                .setAdopterName("testEmployee")
                .setAdoptionDate(getDateTime())
                .builder();
        System.out.println(animal.toString());
        System.out.println(animalAdopted.toString());
        animalDtoService.deleteById(id);
        animalAdoptedService.save(animalAdopted);
        return ResponseEntity.noContent().build();
    }

    private LocalDateTime getDateTime() {
        return LocalDateTime.now();
    }
}
