package com.dg.animalshelter.web.controller;

import com.dg.animalshelter.database.model.Employee;
import com.dg.animalshelter.database.service.EmployeeService;
import com.dg.animalshelter.domain.exception.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@RestController
@RequestMapping("empl")
public class EmployeeRestController {

    @Autowired
    private EmployeeService employeeService;

    @GetMapping
    public ResponseEntity<Collection<Employee>> getEmployees() {
        Collection<Employee> employeeList = employeeService.getAll();
        if (!employeeList.isEmpty()) {
            return ResponseEntity.ok().body(employeeList);
        } else {
            throw new ResourceNotFoundException("Sorry! Employee with provided id not found");
        }
    }

    @PostMapping(value = "/save", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Employee> saveEmployee(@RequestBody Employee employee) {
        if (employee != null) {
            employeeService.save(employee);
            return ResponseEntity.status(201).build();
        }
        return ResponseEntity.unprocessableEntity().body(employee);
    }
}
