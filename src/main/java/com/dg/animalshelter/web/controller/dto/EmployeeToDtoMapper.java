package com.dg.animalshelter.web.controller.dto;

import com.dg.animalshelter.database.model.Employee;

public class EmployeeToDtoMapper {

    public static EmployeeDto mapToDto(Employee employee){
        EmployeeDto employeeDto = new EmployeeDto();
        employeeDto.setEmail(employee.getEmail());
        employeeDto.setName(employee.getName());
        employeeDto.setPassword(employee.getPassword());
        employeeDto.setSurname((employee.getSurname()));
        employeeDto.setId(employee.getId());
        return employeeDto;
    }
}
