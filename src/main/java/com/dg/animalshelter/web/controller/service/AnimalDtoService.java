package com.dg.animalshelter.web.controller.service;

import com.dg.animalshelter.web.controller.dto.AnimalDto;

import java.util.Collection;

public interface AnimalDtoService {
    Collection<AnimalDto> getAll();
    AnimalDto getById(long id);
    AnimalDto save(AnimalDto animal);
    boolean existsById(long id);
    boolean isFull();
    void deleteById(long id);
    AnimalDto update(AnimalDto animalDto);
}
