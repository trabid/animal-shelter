package com.dg.animalshelter.web.controller.dto;

import com.dg.animalshelter.database.model.Animal;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import javax.validation.constraints.NotNull;
import java.util.Objects;

@JsonSerialize
public class AnimalDto {

    private Long id;
    @NotNull
    private String name;
    @NotNull
    private String race;
    private int age;
    private int weight;
    private double price;
    private String image;

    public AnimalDto() {
    }

    private AnimalDto(Builder builder) {
        this.id = builder.id;
        this.name = builder.name;
        this.race = builder.race;
        this.age = builder.age;
        this.weight = builder.weight;
        this.price = builder.price;
        this.image = builder.image;
    }

    public AnimalDto(Animal animal) {
        if(animal.getId() != null)
        this.id =animal.getId();
        this.age = animal.getAge();
        if(animal.getName() != null)
        this.name = animal.getName();
        if(animal.getImage() != null)
        this.image = animal.getImage();
        this.price = animal.getPrice();
        if(animal.getRace() != null)
        this.race = animal.getRace();
        this.weight = animal.getWeight();
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getRace() {
        return race;
    }

    public int getAge() {
        return age;
    }

    public int getWeight() {
        return weight;
    }

    public double getPrice() {
        return price;
    }

    public String getImage() {
        return image;
    }


    @Override
    public String toString() {
        return "Animal{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", race='" + race + '\'' +
                ", age=" + age +
                ", weight=" + weight +
                ", price=" + price +
                ", image='" + image + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AnimalDto animalDto = (AnimalDto) o;
        return age == animalDto.age &&
                Objects.equals(id, animalDto.id) &&
                Objects.equals(name, animalDto.name) &&
                Objects.equals(race, animalDto.race);
    }

    @Override
    public int hashCode() {

        return Objects.hash(name, race, age);
    }

    public static class Builder{
        //        @GeneratedValue(strategy = GenerationType.IDENTITY)
        private Long id;
        @NotNull
        private String name;
        @NotNull
        private String race;
        //        @Size(min = 1, max = 20)
        private int age;
        private int weight;
        private double price;
        private String image;

        public Builder() {
        }

        public Builder (String name, String race) {
            this.name = name;
            this.race = race;
        }

        public AnimalDto.Builder setId(long id) {
            if(id != 0L)
            this.id = id;
            return this;
        }

        public AnimalDto.Builder setAge(int age) {
            if(age != 0)
                this.age = age;
            return this;
        }

        public AnimalDto.Builder setWeight(int weight) {
            if (weight != 0)
                this.weight = weight;
            return this;
        }

        public AnimalDto.Builder setPrice(double price) {
            if (price != 0.0d)
                this.price = price;
            return this;
        }

        public AnimalDto.Builder setImage(String image) {
            if(image != null)
                this.image = image;
            return this;
        }

        public AnimalDto build(){
            return new AnimalDto(this);
        }
    }
}
