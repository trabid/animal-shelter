package com.dg.animalshelter.web.controller.service;

import com.dg.animalshelter.web.controller.dto.EmployeeDto;
import com.dg.animalshelter.database.model.Employee;

import java.util.Collection;

public interface EmployeeDtoService {
    void save(EmployeeDto employeee);
    void update(EmployeeDto employee);
    void delete(Long id);
    Collection<EmployeeDto> getAll();
    Employee getByEmail(String email);
}
