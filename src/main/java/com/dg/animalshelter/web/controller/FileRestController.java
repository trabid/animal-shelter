package com.dg.animalshelter.web.controller;

import com.dg.animalshelter.database.model.DataType;
import com.dg.animalshelter.domain.FileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

@RestController
@RequestMapping("file")
public class FileRestController {

    @Autowired
    private FileService fileService;

    @GetMapping(value = "/pdf")
    public ResponseEntity<Resource> downloadPdf(HttpServletRequest request) {
        return getResourceResponseEntity(request, DataType.PDF);
    }

    @GetMapping("/csv")
    public ResponseEntity<Resource> downloadCsv(HttpServletRequest request) {
        return getResourceResponseEntity(request, DataType.CSV);
    }

    private ResponseEntity<Resource> getResourceResponseEntity(HttpServletRequest request, DataType csv) {
        Resource resource = fileService.loadFileAsResource(csv);

        String contentType = null;
        try {
            contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
        } catch (IOException io) {
            io.printStackTrace();
        }
        if (contentType == null) {
            contentType = "application/octet-stream";
        }
        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(contentType))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
                .body(resource);
    }
}
