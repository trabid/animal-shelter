package com.dg.animalshelter.domain.payment;

public class PaypalStrategy implements PaymentStrategy {
    @Override
    public void pay(int amount) {
        System.out.println(amount + " paid by Paypal");
    }
}
