package com.dg.animalshelter.domain.payment;

public interface PaymentStrategy {
    void pay(int amount);
}
