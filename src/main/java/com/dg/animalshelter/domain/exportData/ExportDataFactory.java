package com.dg.animalshelter.domain.exportData;

import com.dg.animalshelter.database.model.DataType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ExportDataFactory {

    @Autowired
    private List<ExportData> services;

    private static final Map<DataType, ExportData> myServiceCache = new HashMap<>();

    @PostConstruct
    public void initMyServiceCache() {
        for(ExportData service : services) {
            myServiceCache.put(service.getType(), service);
        }
    }

    public static ExportData getExportData(DataType dataType) {
        return myServiceCache.get(dataType);
    }
}
