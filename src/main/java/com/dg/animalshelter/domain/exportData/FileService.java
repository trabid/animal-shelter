package com.dg.animalshelter.domain.exportData;

import com.dg.animalshelter.database.model.DataType;
import org.springframework.core.io.Resource;

public interface FileService {
    Resource loadFileAsResource(DataType dataType);
}
