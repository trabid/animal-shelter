package com.dg.animalshelter.domain.exportData;

import com.dg.animalshelter.database.model.Animal;
import com.dg.animalshelter.database.model.DataType;
import com.dg.animalshelter.database.service.AnimalService;
import com.dg.animalshelter.database.service.SettingService;
import org.springframework.stereotype.Component;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

@Component
public class ExportDataCSV implements ExportData {
    private AnimalService animalService;
    private SettingService settingService;

    public ExportDataCSV(AnimalService animalService, SettingService settingService) {
        this.animalService = animalService;
        this.settingService = settingService;
    }

    @Override
    public void export() throws Exception {
        List<Animal> animals = new ArrayList<Animal>(animalService.getAll());
        Writer writer = null;
        StringBuilder sb;
        String separator = ";";

        String path = settingService.getCSVPath();
        writer = getWriter(path);
        String head = "ID;NAME;AGE;RACE;WEIGHT;IMAGE\n";
        generateCSV(animals, writer, separator, head);
    }

    @Override
    public DataType getType() {
        return DataType.CSV;
    }

    private void generateCSV(List<Animal> animals, Writer writer, String separator, String head) throws IOException {
        StringBuilder sb;
        writer.write(head);
        for (Animal animal : animals) {
            sb = new StringBuilder();
            sb.append(animal.getId());
            sb.append(separator);
            sb.append(animal.getName());
            sb.append(separator);
            sb.append(animal.getAge());
            sb.append(separator);
            sb.append(animal.getRace());
            sb.append(separator);
            sb.append(animal.getWeight());
            sb.append(separator);
            sb.append(animal.getImage());
            sb.append("\n");
            writer.write(sb.toString());
            System.out.println(sb.toString());
        }
        writer.flush();
        writer.close();
    }

    private Writer getWriter(String path) throws IOException {
        Writer writer;
        File file = new File(path);
        writer = new BufferedWriter(new FileWriter(file));
        return writer;
    }

}
