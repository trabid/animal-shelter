package com.dg.animalshelter.domain.exportData;

import com.dg.animalshelter.database.model.DataType;

public interface ExportData {
    void export() throws Exception;
    DataType getType();
}
