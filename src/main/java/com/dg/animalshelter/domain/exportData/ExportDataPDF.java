package com.dg.animalshelter.domain.exportData;

import com.dg.animalshelter.database.model.Animal;
import com.dg.animalshelter.database.service.AnimalService;
import com.dg.animalshelter.database.service.SettingService;
import com.itextpdf.text.*;
import com.itextpdf.text.Font;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.dg.animalshelter.database.model.DataType;
import org.springframework.stereotype.Component;

import java.awt.*;
import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;

@Component
public class ExportDataPDF implements ExportData {
    private AnimalService animalService;
    private SettingService settingService;

    public ExportDataPDF(AnimalService animalService, SettingService settingService) {
        this.animalService = animalService;
        this.settingService = settingService;
    }

    @Override
    public void export() throws Exception {
        String path = settingService.getPDFPath();
        List<Animal> animalList = new ArrayList<>(animalService.getAll());
        String[] header = {"ID", "NAME", "AGE", "RACE", "WEIGHT", "IMAGE"};
        String[][] body = getBody(animalList, header);
        savePDF(path, header, body);
    }

    private String[][] getBody(List<Animal> animalList, String[] header) {
        String[][] body = new String[header.length][animalList.size()];
        for (int row = 0; row < animalList.size()-1; row++) {
            String[] temp = getAnimalArray(animalList.get(row), header.length);
            for (int col = 0; col < header.length-1; col++) {
                body[row][col] = temp[col];
            }
        }
        return body;
    }

    @Override
    public DataType getType() {
        return DataType.PDF;
    }

    private void savePDF(String path, String header[], String body[][]) throws Exception {
        Document document = new Document();
        File file = new File(path);
        file.createNewFile();
        FileOutputStream fop = new FileOutputStream(file);
        PdfWriter.getInstance(document, fop);
        document.open();
        PdfPTable table = generatePDFTable(header, body, document);
        finishDocument(document, table);
        fop.flush();
        fop.close();
    }

    private void finishDocument(Document document, PdfPTable table) throws DocumentException {
        document.add(table);
        document.close();
    }

    private PdfPTable generatePDFTable(String[] header, String[][] body, Document document) throws DocumentException {
        Font fontHeader = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD);
        Font fontBody = new Font(Font.FontFamily.COURIER, 12, Font.NORMAL);
        tableForHeader(header, document, fontHeader);
        PdfPTable table = getPdfPTable(header, body, fontBody);
        return table;
    }

    private PdfPTable getPdfPTable(String[] header, String[][] body, Font fontBody) {
        PdfPTable table = new PdfPTable(header.length);
        for (int i = 0; i < body.length; i++) {
            for (int j = 0; j < body[i].length; j++) {
                table.addCell(new Phrase(body[i][j], fontBody));
            }
        }
        return table;
    }

    private void tableForHeader(String[] header, Document document, Font fontHeader) throws DocumentException {
        PdfPTable headerTable = new PdfPTable(header.length);
        for (String h : header) {
            Phrase frase = new Phrase(h, fontHeader);
            PdfPCell cell = new PdfPCell(frase);
            cell.setBackgroundColor(new BaseColor(Color.lightGray.getRGB()));
            headerTable.addCell(cell);
        }
        document.add(headerTable);
    }

    private String[] getAnimalArray(Animal animal, int length) {
        String[] arr = new String[length];
        arr[0] = animal.getId().toString();
        arr[1] = animal.getName();
        arr[2] = String.valueOf(animal.getAge());
        arr[3] = animal.getRace();
        arr[4] = String.valueOf(animal.getWeight());
        arr[5] = animal.getImage();
        return arr;
    }
}
