package com.dg.animalshelter.domain.exportData;

import com.dg.animalshelter.domain.exception.ResourceNotFoundException;
import com.dg.animalshelter.database.model.DataType;
import com.dg.animalshelter.database.service.SettingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Component;

import java.net.MalformedURLException;
import java.nio.file.Path;
import java.nio.file.Paths;

@Component
public class FileServiceImpl implements FileService {

    @Autowired
    private SettingService settingService;
    private ExportData exportData;

    public FileServiceImpl() {
    }

    @Override
    public Resource loadFileAsResource(DataType dataType) {
        exportData = ExportDataFactory.getExportData(dataType);
        try {
            exportData.export();
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            Path path;
            if (dataType == DataType.CSV) {
                path = Paths.get(settingService.getCSVPath());
            } else {
                path = Paths.get(settingService.getPDFPath());
            }
            Resource resource = new UrlResource(path.toUri());
            if (resource.exists()) {
                return resource;
            } else {
                throw new ResourceNotFoundException(dataType + " file not found");
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
            throw new ResourceNotFoundException(dataType + " file not found", e);
        }
    }
}
