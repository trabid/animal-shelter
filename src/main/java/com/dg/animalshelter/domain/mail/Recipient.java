package com.dg.animalshelter.domain.mail;

public class Recipient {
    private final String mail;

    public Recipient(String mail) {
        this.mail = mail;
    }

    public String getMail() {
        return mail;
    }
}
