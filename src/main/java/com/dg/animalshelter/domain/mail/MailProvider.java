package com.dg.animalshelter.domain.mail;

public interface MailProvider {
    void sendMail(Mail mail);
}
