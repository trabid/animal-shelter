package com.dg.animalshelter.domain.mail;

import com.dg.animalshelter.database.model.MailEntity;

import java.io.File;

public class Mail {
    private String sender;
    private String recipient;
    private String subject;
    private String message;
    private File attachment;

    public Mail() {
    }

    public Mail(Builder builder) {
        this.sender = builder.sender;
        this.recipient = builder.recipient;
        this.subject = builder.subject;
        this.message = builder.message;
        this.attachment = builder.attachment;
    }

    public MailEntity toDto() {
        return new MailEntity.Builder( new Recipient(this.recipient),new Sender(this.sender))
                .setSubject(this.subject)
                .setMessage(this.message)
                .setAttachment(this.attachment)
                .build();
    }

    public String getSender() {
        return sender;
    }

    public String getRecipient() {
        return recipient;
    }

    public String getSubject() {
        return subject;
    }

    public String getMessage() {
        return message;
    }

    public File getAttachment() {
        return attachment;
    }


    public static class Builder {
        private String sender;
        private String recipient;
        private String subject;
        private String message;
        private File attachment;

        public Builder(String sender, String recipient) {
            this.sender = sender;
            this.recipient = recipient;
        }

        public Builder setSubject(String subject) {
            this.subject = subject;
            return this;
        }

        public Builder setMessage(String message) {
            this.message = message;
            return this;
        }

        public Builder setAttachment(File attachment) {
            this.attachment = attachment;
            return this;
        }

        public Mail build() {
            return new Mail(this);
        }
    }
}
