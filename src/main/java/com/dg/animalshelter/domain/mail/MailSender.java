package com.dg.animalshelter.domain.mail;

public class MailSender {
    private MailProvider mailProvider;

    public MailSender() {
        this.mailProvider = new SmtpMailProvider();
    }

    public MailSender(MailProvider mailProvider) {
        this.mailProvider = mailProvider;
    }

    public void setMailProvider(MailProvider mailProvider) {
        this.mailProvider = mailProvider;
    }

    public void sendMail(Mail mail) {
        if (mailProvider == null) {
            throw new RuntimeException("Need mail provider to send e-mail");
        } else {
            mailProvider.sendMail(mail);
        }
    }
}
