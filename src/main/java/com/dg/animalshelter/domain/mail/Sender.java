package com.dg.animalshelter.domain.mail;

public class Sender {
    private final String mail;

    public Sender(String mail) {
        this.mail = mail;
    }

    public String getMail() {
        return mail;
    }
}
