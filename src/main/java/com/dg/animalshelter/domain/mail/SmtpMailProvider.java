package com.dg.animalshelter.domain.mail;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import java.io.IOException;
import java.util.Properties;

public class SmtpMailProvider implements MailProvider {
    private final String user = "0a79b79b2fce4b";
    private final String password = "fa7060fc6913c4";

    @Override
    public void sendMail(Mail mail) {
        try {
            Session session = getSession(getProps());
            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(mail.getSender()));
            message.setRecipients(
                    Message.RecipientType.TO, InternetAddress.parse(mail.getRecipient()));
            message.setSubject(mail.getSubject());

            MimeBodyPart mimeBodyPart = new MimeBodyPart();
            mimeBodyPart.setContent(mail.getMessage(), "text/html");

            MimeBodyPart attachmentMimeBodyPart = new MimeBodyPart();
            mimeBodyPart.attachFile(mail.getAttachment());

            Multipart multipart = new MimeMultipart();
            multipart.addBodyPart(mimeBodyPart);
            multipart.addBodyPart(attachmentMimeBodyPart);

            message.setContent(multipart);

            Transport.send(message);
        } catch (MessagingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private Session getSession(Properties prop) {
        return Session.getInstance(prop, new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(user, password);
            }
        });
    }

    private Properties getProps() {
        Properties props = new Properties();
        props.put("mail.smtp.auth", true);
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.mailtrap.io");
        props.put("mail.smtp.port", "2525");
        props.put("mail.smtp.ssl.trust", "smtp.mailtrap.io");
        return props;
    }

}
