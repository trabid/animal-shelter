package com.dg.animalshelter.database.repository;

import com.dg.animalshelter.database.model.MailEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MailRepository  extends JpaRepository<MailEntity, Long> {
}
