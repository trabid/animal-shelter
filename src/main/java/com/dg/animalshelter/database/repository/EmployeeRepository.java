package com.dg.animalshelter.database.repository;

import com.dg.animalshelter.database.model.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Long> {
    Employee findEmployeeByEmail(String email);
    boolean existsByEmail(String email);
}
