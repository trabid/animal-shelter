package com.dg.animalshelter.database.repository;

import com.dg.animalshelter.database.model.Payment;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PaymentRepository extends JpaRepository<Payment, Long> {
}
