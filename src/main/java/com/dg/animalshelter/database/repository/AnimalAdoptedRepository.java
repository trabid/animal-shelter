package com.dg.animalshelter.database.repository;

import com.dg.animalshelter.database.model.AnimalAdopted;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AnimalAdoptedRepository extends JpaRepository<AnimalAdopted, Long> {
}
