package com.dg.animalshelter.database.service;

import com.dg.animalshelter.database.repository.AnimalAdoptedRepository;
import com.dg.animalshelter.database.model.AnimalAdopted;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class AnimalAdoptedServiceImpl implements AnimalAdoptedService {

    @Autowired
    private AnimalAdoptedRepository animalAdoptedRepository;

    @Override
    public void save(AnimalAdopted animalAdopted) {
        animalAdoptedRepository.save(animalAdopted);
    }

    @Override
    public Collection<AnimalAdopted> getAll() {
        return animalAdoptedRepository.findAll();
    }
}
