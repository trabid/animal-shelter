package com.dg.animalshelter.database.service;

import com.dg.animalshelter.database.model.MailEntity;
import com.dg.animalshelter.database.repository.MailRepository;
import com.dg.animalshelter.domain.exception.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class MailServiceImpl implements MailService {

    @Autowired
    private MailRepository mailRepository;

    @Override
    public void save(MailEntity mailEntity) {
            mailRepository.save(mailEntity);
    }

    @Override
    public void delete(long id) {
        if(mailRepository.existsById(id)){
            mailRepository.deleteById(id);
        } else {
            throw new ResourceNotFoundException("Mail not found");
        }
    }

    @Override
    public Collection<MailEntity> getAll() {
        if(!mailRepository.findAll().isEmpty()) {
            return mailRepository.findAll();
        } else {
            throw new ResourceNotFoundException("Mails not found");
        }
    }

    @Override
    public MailEntity getById(long id) {
        if(mailRepository.existsById(id)){
           return mailRepository.getOne(id);
        } else {
            throw new ResourceNotFoundException("Mail not found");
        }    }
}
