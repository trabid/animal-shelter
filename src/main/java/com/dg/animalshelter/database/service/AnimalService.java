package com.dg.animalshelter.database.service;


import com.dg.animalshelter.database.model.Animal;

import java.util.Collection;

public interface AnimalService {
    Collection<Animal> getAll();
    Animal getById(long id);
    Animal save(Animal animal);
    boolean existsById(long id);
    boolean isFull();
    void deleteById(long id);
    Animal update(Animal animal);
}
