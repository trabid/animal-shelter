package com.dg.animalshelter.database.service;

import com.dg.animalshelter.database.model.Settings;
import com.dg.animalshelter.database.repository.SettingsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.NoSuchElementException;

@Service
public class SettingServiceImpl implements SettingService {
    private static final String CSV_PATH_NAME = "csv path";
    private static final String PDF_PATH_NAME = "pdf path";

    @Autowired
    private SettingsRepository settingsRepository;

//    @Autowired
//    public SettingServiceImpl(SettingsRepository settingsRepository) {
//        this.settingsRepository = settingsRepository;
//    }

    @Override
    public String getCSVPath() {
        String path;
        path = getCsvPathName();
        if (path == null) {
            path = System.getProperty("user.home") + "//" + "animals.csv";
        }
        return path;
    }


    @Override
    public void setCSVPath(String path) {
        Settings settings = new Settings(CSV_PATH_NAME, path);
        settingsRepository.save(settings);
    }

    @Override
    public String getPDFPath() {
        String path;
        path = getPdfPathName();
        if (path == null) {
            path = System.getProperty("user.home") + "//" + "animals.pdf";
        }
        return path;
    }


    @Override
    public void setPDFPath(String path) {
        Settings settings = new Settings(PDF_PATH_NAME, path);
        settingsRepository.save(settings);
    }

    private boolean isCSVPathPresent(List<Settings> list) {
        try {
            return list.stream().filter(x -> x.getName().toLowerCase().equals(CSV_PATH_NAME)).findFirst().get().getValue() != null;
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    private String getCSVPathValue(List<Settings> settings) {
        return settings.stream()
                .filter(x -> x.getName().toLowerCase().equals(CSV_PATH_NAME))
                .findFirst()
                .get()
                .getValue();
    }

    private boolean isPDFPathPresent(List<Settings> list) {
        try {
            return list.stream().filter(x -> x.getName().toLowerCase().equals(PDF_PATH_NAME)).findFirst().get().getValue() != null;
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    private String getPdfPathValue(List<Settings> settings) {
        return settings.stream()
                .filter(x -> x.getName().toLowerCase().equals(PDF_PATH_NAME))
                .findFirst()
                .get()
                .getValue();
    }

    private String getCsvPathName() {
        List<Settings> settings = settingsRepository.findAll();
        if (isCSVPathPresent(settings)) {
            return getCSVPathValue(settings);
        } else {
            return null;
        }
    }

    private String getPdfPathName(){
        List<Settings> settings = settingsRepository.findAll();
        if (isPDFPathPresent(settings)) {
            return getPdfPathValue(settings);
        } else {
            return null;
        }
    }
}
