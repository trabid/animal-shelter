package com.dg.animalshelter.database.service;

import com.dg.animalshelter.database.model.AnimalAdopted;

import java.util.Collection;

public interface AnimalAdoptedService {
    void save(AnimalAdopted animalAdopted);
    Collection<AnimalAdopted> getAll();
}
