package com.dg.animalshelter.database.service;

import com.dg.animalshelter.database.model.Payment;

import java.util.Collection;

public interface PaymentService {
    Collection<Payment> getAll();
    void save(Payment payment);
}
