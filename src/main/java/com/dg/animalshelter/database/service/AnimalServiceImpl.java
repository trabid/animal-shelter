package com.dg.animalshelter.database.service;

import com.dg.animalshelter.database.model.Animal;
import com.dg.animalshelter.database.repository.AnimalRepository;
import com.dg.animalshelter.domain.exception.ResourceNotFoundException;
import com.dg.animalshelter.database.model.Capacity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Comparator;
import java.util.stream.Collectors;

@Service
public class AnimalServiceImpl implements AnimalService {
    int cap = Capacity.SMALL.get();
    @Autowired
    AnimalRepository repository;

    @Override
    public Collection<Animal> getAll() {
        return repository.findAll().stream()
                .sorted(Comparator.comparing(Animal::getName))
                .collect(Collectors.toList());
    }

    @Override
    public Animal getById(long id) {
        if (existsById(id)) {
            return repository.getOne(id);
        }
        throw new ResourceNotFoundException("Sorry! AnimalAdopted with id:" + id + " not found");
    }

    @Override
    public Animal save(Animal animal) {
        if (!isFull()) {
            return repository.save(animal);
        }
        throw new IndexOutOfBoundsException("Shelter is full");
    }

    @Override
    public boolean existsById(long id) {
        return repository.existsById(id);
    }

    @Override
    public boolean isFull() {
        return (repository.findAll().size() >= cap) ? true : false;
    }

    @Override
    public void deleteById(long id) {
        if (existsById(id)) {
            repository.deleteById(id);
        } else {
            throw new ResourceNotFoundException("AnimalAdopted with id:" + id + " not found");
        }
    }

    @Override
    public Animal update(Animal animal) {
        if (repository.existsById(animal.getId())) {
            return repository.save(animal);
        }
        throw new ResourceNotFoundException("AnimalAdopted with id:" + animal.getId() + " not found");
    }
}
