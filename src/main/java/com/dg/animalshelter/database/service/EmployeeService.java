package com.dg.animalshelter.database.service;

import com.dg.animalshelter.database.model.Employee;

import java.util.Collection;

public interface EmployeeService {
    void save(Employee employeee);
    void update(Employee employee);
    void delete(Long id);
    Collection<Employee> getAll();
    Employee getByEmail(String email);
}
