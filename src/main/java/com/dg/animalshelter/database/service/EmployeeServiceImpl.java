package com.dg.animalshelter.database.service;

import com.dg.animalshelter.domain.exception.ResourceNotFoundException;
import com.dg.animalshelter.database.model.Employee;
import com.dg.animalshelter.database.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class EmployeeServiceImpl  implements EmployeeService{

    @Autowired
    private EmployeeRepository employeeRepository;

    @Override
    public void save(Employee employee) {
        employeeRepository.save(employee);
    }

    @Override
    public void update(Employee employee) {
        if(employeeRepository.existsById(employee.getId())) {
            employeeRepository.save(employee);
        } else {
            throw new ResourceNotFoundException("Employee with id: "+employee.getId()+" not found");
        }
    }

    @Override
    public void delete(Long id) {
        if(employeeRepository.existsById(id)) {
        employeeRepository.deleteById(id);
    } else {
        throw new ResourceNotFoundException("Employee with id: "+id+" not found");
    }
    }

    @Override
    public Collection<Employee> getAll() {
        return employeeRepository.findAll();
    }

    @Override
    public Employee getByEmail(String email) {
        if(employeeRepository.existsByEmail(email)){
            return employeeRepository.findEmployeeByEmail(email);
        } else {
            throw new ResourceNotFoundException("Emplee with email: " + email + " not dfound");
        }
    }
}
