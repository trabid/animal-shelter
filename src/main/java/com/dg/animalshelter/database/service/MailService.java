package com.dg.animalshelter.database.service;

import com.dg.animalshelter.database.model.MailEntity;

import java.util.Collection;

public interface MailService {
    void save(MailEntity mailEntity);
    void delete(long id);
    Collection<MailEntity> getAll();
    MailEntity getById(long id);
}
