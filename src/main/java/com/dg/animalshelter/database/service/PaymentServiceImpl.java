package com.dg.animalshelter.database.service;

import com.dg.animalshelter.database.model.Payment;
import com.dg.animalshelter.database.repository.PaymentRepository;
import com.dg.animalshelter.domain.payment.CreditCardStrategy;
import com.dg.animalshelter.domain.payment.PaypalStrategy;
import com.dg.animalshelter.domain.payment.ShoppingCart;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.support.NullValue;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import javax.transaction.TransactionalException;
import java.util.Collection;

@Service
public class PaymentServiceImpl implements PaymentService {

    private PaymentRepository paymentRepository;

    @Autowired
    public PaymentServiceImpl(PaymentRepository paymentRepository) {
        this.paymentRepository = paymentRepository;
    }

    @Override
    public Collection<Payment> getAll() {
        return paymentRepository.findAll();
    }

    @Override
    @Transactional
    public void save(Payment payment) {
        if (payment != null) {
            paymentRepository.save(payment);
            ShoppingCart sc = new ShoppingCart();
            if(payment.getSubject().equals("Karta"))
            sc.pay(new CreditCardStrategy(payment.getPayerName(),payment.getAmount().toString(),payment.getPayerAccountNumber().toString(),payment.getSubject()));
            else {
                sc.pay(new PaypalStrategy());
            }
        } else
            throw new TransactionalException("Payment can not be realised", new Throwable());
    }
}
