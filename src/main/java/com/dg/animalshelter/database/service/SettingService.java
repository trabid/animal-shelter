package com.dg.animalshelter.database.service;

public interface SettingService {
    String getCSVPath();
    void setCSVPath(String path);
    String getPDFPath();
    void setPDFPath(String path);
}
