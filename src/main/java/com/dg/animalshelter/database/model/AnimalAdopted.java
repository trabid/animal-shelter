package com.dg.animalshelter.database.model;

import com.dg.animalshelter.web.controller.dto.AnimalDto;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.Objects;

@Entity
public class AnimalAdopted {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;
    @NotNull
    private String name;
    @NotNull
    private String race;
    private int age;
    private int weight;
    private double price;
    private String image;
    private LocalDateTime adoptionDate;
    private String adopterName;

    public AnimalAdopted() {
    }

    private AnimalAdopted(AnimalAdopted.Builder builder) {
        this.id = builder.id;
        this.age = builder.age;
        this.image = builder.image;
        this.name = builder.name;
        this.race = builder.race;
        this.price = builder.price;
        this.weight = builder.weight;
        this.adoptionDate = builder.adoptionDate;
        this.adopterName = builder.adopterName;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getRace() {
        return race;
    }

    public int getAge() {
        return age;
    }

    public int getWeight() {
        return weight;
    }

    public double getPrice() {
        return price;
    }

    public String getImage() {
        return image;
    }

    public LocalDateTime getAdoptionDate() {
        return adoptionDate;
    }

    public String getAdopterName() {
        return adopterName;
    }

    @Override
    public String toString() {
        return "AnimalAdopted{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", race='" + race + '\'' +
                ", age=" + age +
                ", weight=" + weight +
                ", price=" + price +
                ", image='" + image + '\'' +
                ", adoptionDate=" + adoptionDate +
                ", adopterName='" + adopterName + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        AnimalAdopted that = (AnimalAdopted) o;
        return Objects.equals(adoptionDate, that.adoptionDate) &&
                Objects.equals(adopterName, that.adopterName);
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), adoptionDate, adopterName);
    }

    public static class Builder {

        private Long id;
        private String name;
        private String race;
        private int age;
        private int weight;
        private double price;
        private String image;
        private LocalDateTime adoptionDate;
        private String adopterName;

        public Builder(Animal animal) {
            this.id = animal.getId();
            this.name = animal.getName();
            this.race = animal.getRace();
            this.age = animal.getAge();
            this.weight = animal.getWeight();
            this.price = animal.getPrice();
            this.image = animal.getImage();
        }

        public Builder(AnimalDto animal) {
            this.id = animal.getId();
            this.name = animal.getName();
            this.race = animal.getRace();
            this.age = animal.getAge();
            this.weight = animal.getWeight();
            this.price = animal.getPrice();
            this.image = animal.getImage();
        }

//        public AnimalAdopted.Builder setId(long id) {
////            if(id != 0L)
////            this.id = id;
//            return this;
//        }
//
//        public AnimalAdopted.Builder setAge(int age) {
//            if(age != 0)
//                this.age = age;
//            return this;
//        }
//
//        public AnimalAdopted.Builder setWeight(int weight) {
//            if (weight != 0)
//                this.weight = weight;
//            return this;
//        }
//
//        public AnimalAdopted.Builder setPrice(double price) {
//            if (price != 0.0d)
//                this.price = price;
//            return this;
//        }
//
//        public AnimalAdopted.Builder setImage(String image) {
//            if(image != null)
//                this.image = image;
//            return this;
//        }

        public AnimalAdopted.Builder setAdoptionDate(LocalDateTime adoptionDate) {
            if (adoptionDate != null)
                this.adoptionDate = adoptionDate;
            return this;
        }

        public AnimalAdopted.Builder setAdopterName(String adopterName) {
            if (adopterName != null) {
                this.adopterName = adopterName;
            }
            return this;
        }

        public AnimalAdopted builder() {
            return new AnimalAdopted(this);
        }
    }
}
