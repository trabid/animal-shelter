package com.dg.animalshelter.database.model;

public enum Capacity {
    SMALL(20),MEDIUM(50),BIG(100);
    private final int value;

    private Capacity(int value) {
        this.value = value;
    }

    public int get(){
        return value;
    }
}
