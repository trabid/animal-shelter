package com.dg.animalshelter.database.model;

import com.dg.animalshelter.domain.mail.Recipient;
import com.dg.animalshelter.domain.mail.Sender;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.io.File;
import java.util.Objects;

@Entity
public class MailEntity {
    @Id
    @GeneratedValue
    private Long id;
    private String sender;
    private String recipient;
    private String subject;
    private String message;
    private File attachment;

    public MailEntity() {
    }

    public MailEntity(Builder builder) {
        this.sender = builder.sender;
        this.recipient = builder.recipient;
        this.subject = builder.subject;
        this.message = builder.message;
        this.attachment = builder.attachment;
    }


    public Long getId() {
        return id;
    }

    public String getSender() {
        return sender;
    }

    public String getRecipient() {
        return recipient;
    }

    public String getSubject() {
        return subject;
    }

    public String getMessage() {
        return message;
    }

    public File getAttachment() {
        return attachment;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MailEntity that = (MailEntity) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(sender, that.sender) &&
                Objects.equals(recipient, that.recipient) &&
                Objects.equals(subject, that.subject) &&
                Objects.equals(message, that.message) &&
                Objects.equals(attachment, that.attachment);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, sender, recipient, subject, message, attachment);
    }

    @Override
    public String toString() {
        return "MailEntity{" +
                "id=" + id +
                ", sender='" + sender + '\'' +
                ", recipient='" + recipient + '\'' +
                ", subject='" + subject + '\'' +
                ", message='" + message + '\'' +
                ", attachment=" + attachment +
                '}';
    }

    public static class Builder {
        private String sender;
        private String recipient;
        private String subject;
        private String message;
        private File attachment;

        public Builder(Recipient recipient, Sender sender) {
            this.sender = sender.getMail();
            this.recipient = recipient.getMail();
        }

//        public MailEntity.Builder setSender(String sender) {
//            this.sender=sender;
//            return this;
//        }
//
//        public MailEntity.Builder setRecipient(String recipient) {
//            this.recipient=recipient;
//            return this;
//        }

        public MailEntity.Builder setSubject(String subject) {
            this.subject = subject;
            return this;
        }

        public MailEntity.Builder setMessage(String message) {
            this.message = message;
            return this;
        }

        public MailEntity.Builder setAttachment(File attachment) {
            this.attachment = attachment;
            return this;
        }

        public MailEntity build() {
            return new MailEntity(this);
        }
    }
}
