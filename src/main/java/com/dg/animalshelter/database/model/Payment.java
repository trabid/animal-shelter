package com.dg.animalshelter.database.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.math.BigDecimal;
import java.util.Objects;

@Entity
public class Payment {

    @Id
    @GeneratedValue
    private Long id;
    private String payerName;
    private String subject;
    private BigDecimal amount;
    private Long payerAccountNumber;


    @Override
    public String toString() {
        return "Payment{" +
                "id=" + id +
                ", payerName='" + payerName + '\'' +
                ", subject='" + subject + '\'' +
                ", amount=" + amount +
                ", payerAccountNumber=" + payerAccountNumber +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Payment payment = (Payment) o;
        return Objects.equals(id, payment.id);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getPayerAccountNumber() {
        return payerAccountNumber;
    }

    public void setPayerAccountNumber(Long payerAccountNumber) {
        this.payerAccountNumber = payerAccountNumber;
    }

    public String getPayerName() {
        return payerName;
    }

    public void setPayerName(String payerName) {
        this.payerName = payerName;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }
}
