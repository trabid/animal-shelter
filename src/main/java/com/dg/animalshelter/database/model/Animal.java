package com.dg.animalshelter.database.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Objects;

@Entity
public class Animal implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;
    @NotNull
    private String name;
    @NotNull
    private String race;
    private int age;
    private int weight;
    private double price;
    private String image;

    public Animal() {
    }

    private Animal(Builder builder) {
        this.name = builder.name;
        this.race = builder.race;
        this.age = builder.age;
        this.weight = builder.weight;
        this.price = builder.price;
        this.image = builder.image;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getRace() {
        return race;
    }

    public int getAge() {
        return age;
    }

    public int getWeight() {
        return weight;
    }

    public double getPrice() {
        return price;
    }

    public String getImage() {
        return image;
    }


    @Override
    public String toString() {
        return "Animal{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", race='" + race + '\'' +
                ", age=" + age +
                ", weight=" + weight +
                ", price=" + price +
                ", image='" + image + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Animal animal = (Animal) o;
        return age == animal.age &&
                Objects.equals(id, animal.id) &&
                Objects.equals(name, animal.name) &&
                Objects.equals(race, animal.race);
    }

    @Override
    public int hashCode() {

        return Objects.hash(name, race, age);
    }

    public static class Builder{
//        @GeneratedValue(strategy = GenerationType.IDENTITY)
//        private Long id;
        @NotNull
        private String name;
        @NotNull
        private String race;
//        @Size(min = 1, max = 20)
        private int age;
        private int weight;
        private double price;
        private String image;

        public Builder() {
        }

        public Builder (String name, String race) {
            this.name = name;
            this.race = race;
        }

        public Builder setId(long id) {
//            if(id != 0L)
//            this.id = id;
            return this;
        }

        public Builder setAge(int age) {
            if(age != 0)
            this.age = age;
            return this;
        }

        public Builder setWeight(int weight) {
            if (weight != 0)
            this.weight = weight;
            return this;
        }

        public Builder setPrice(double price) {
            if (price != 0.0d)
            this.price = price;
            return this;
        }

        public Builder setImage(String image) {
            if(image != null)
            this.image = image;
            return this;
        }

        public Animal build(){
            return new Animal(this);
        }
    }
}