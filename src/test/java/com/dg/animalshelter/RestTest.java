package com.dg.animalshelter;


import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;


import com.dg.animalshelter.database.model.Animal;
import io.restassured.RestAssured;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
@ContextConfiguration
public class RestTest {

    @Before
    public void setup(){
        RestAssured.baseURI = "http://localhost";
        RestAssured.port = 8080;
    }

    @Test
    public void givenAnimalIsNotExists_whenIfnoIsRetrieved_then404IsRetrieved() {
        String animalId = "1";
        get("/animals/get/"+animalId).then()
                .statusCode(404);
    }

    @Test
    public void givenAnimalIsExists_whenIfnoIsRetrieved_then200IsRetrieved() {
        String animalId = "10";
        get("/animals/get/"+animalId).then()
                .statusCode(200);
    }
    @Test

    public void givenAnimalIsExists_whenIfnoIsRetrieved_thenAssertName() {
        String animalId = "10";
        get("/animals/get/"+animalId).then().log().body()
                .statusCode(200)
        .assertThat().body("name", equalTo("Szarik"));
    }

    @Test
    public void whenRequestPostAnimal_thenStatusCreated() {
        with().body(new Animal.Builder("burek","mieszaniec").setAge(5).build())
                .when()
                .request("post","animals/save")
                .then()
                .log()
                .ifError()
                .statusCode(201);
    }


}
