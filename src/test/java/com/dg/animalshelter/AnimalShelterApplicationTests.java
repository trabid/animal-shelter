package com.dg.animalshelter;

import com.dg.animalshelter.web.controller.Catalog;
import com.dg.animalshelter.database.model.Animal;
import com.dg.animalshelter.domain.ExportData;
import com.dg.animalshelter.database.model.AnimalAdopted;
import com.dg.animalshelter.database.service.AnimalService;
import com.dg.animalshelter.domain.ExportDataFactory;
import com.dg.animalshelter.database.model.DataType;
import com.dg.animalshelter.domain.payment.CreditCardStrategy;
import com.dg.animalshelter.domain.payment.Item;
import com.dg.animalshelter.domain.payment.ShoppingCart;
//import org.apache.http.client.ClientProtocolException;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.io.IOException;
import java.time.LocalDateTime;

//@RunWith(SpringRunner.class)
//@SpringBootTest
public class AnimalShelterApplicationTests {

    @Autowired
    private AnimalService animalService;

    @Test
    public void exportCSV() {
        ExportData ex = ExportDataFactory.getExportData(DataType.CSV);
        try {
            ex.export();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void payment() {
        ShoppingCart cart = new ShoppingCart();

        Item item1 = new Item("1234", 10);
        Item item2 = new Item("5678", 40);

        cart.addItem(item1);
        cart.addItem(item2);

        //pay by credit card
        cart.pay(new CreditCardStrategy("Damian Gręda", "1234567890123456", "786", "12/15"));

    }

    @Test
    public void addAnimalToDatabase() {
        Animal burek = new Animal.Builder("burek","korgi").setAge(2).build();
        animalService.save(burek);
        Assert.assertEquals(true,animalService.existsById(burek.getId()));
        animalService.deleteById(burek.getId());
    }

    @Test
    public void deleteAnimalFromDatabase() {
        Animal burek = new Animal.Builder("burek","korgi").setAge(2).build();
        animalService.save(burek);
        animalService.deleteById(burek.getId());
        Assert.assertEquals(false,animalService.existsById(burek.getId()));
    }

    @Test
    public void givenUserDoesNotExists_whenUserInfoIsRetrieved_then404IsReceived()
            throws  IOException {
//
//        // Given
//        String name = "1";
//        HttpUriRequest request = new HttpGet( "https://api.github.com/users/" + name );
//
//        // When
//        HttpResponse httpResponse = HttpClientBuilder.create().build().execute( request );
//
//        // Then
//        assertThat(
//                httpResponse.getStatusLine().getStatusCode(),
//                equalTo(HttpStatus.SC_NOT_FOUND));
    }

    @Test
    public void adopt() {
        Animal burek = new Animal.Builder("burek","korgi").setAge(2).build();
        AnimalAdopted animalAdopted = new AnimalAdopted.Builder(burek)
                .setAdoptionDate(LocalDateTime.now())
                .setAdopterName("employee")
                .builder();
        System.out.println(burek);
        System.out.println(animalAdopted);
    }

    @Test
    public void catalog(){
        String s =
                "<prod><name>drill</name><prx>99</prx><qty>5</qty></prod>\n\n" +
                        "<prod><name>hammer</name><prx>10</prx><qty>50</qty></prod>\n\n" +
                        "<prod><name>screwdriver</name><prx>5</prx><qty>51</qty></prod>\n\n" +
                        "<prod><name>table saw</name><prx>1099.99</prx><qty>5</qty></prod>\n\n" +
                        "<prod><name>saw</name><prx>9</prx><qty>10</qty></prod>\n\n" +
                        "<prod><name>chair</name><prx>100</prx><qty>20</qty></prod>\n\n" +
                        "<prod><name>fan</name><prx>50</prx><qty>8</qty></prod>\n\n" +
                        "<prod><name>wire</name><prx>10.8</prx><qty>15</qty></prod>\n\n" +
                        "<prod><name>battery</name><prx>150</prx><qty>12</qty></prod>\n\n" +
                        "<prod><name>pallet</name><prx>10</prx><qty>50</qty></prod>\n\n" +
                        "<prod><name>wheel</name><prx>8.80</prx><qty>32</qty></prod>\n\n" +
                        "<prod><name>extractor</name><prx>105</prx><qty>17</qty></prod>\n\n" +
                        "<prod><name>bumper</name><prx>150</prx><qty>3</qty></prod>\n\n" +
                        "<prod><name>ladder</name><prx>112</prx><qty>12</qty></prod>\n\n" +
                        "<prod><name>hoist</name><prx>13.80</prx><qty>32</qty></prod>\n\n" +
                        "<prod><name>platform</name><prx>65</prx><qty>21</qty></prod>\n\n" +
                        "<prod><name>car wheel</name><prx>505</prx><qty>7</qty></prod>\n\n" +
                        "<prod><name>bicycle wheel</name><prx>150</prx><qty>11</qty></prod>\n\n" +
                        "<prod><name>big hammer</name><prx>18</prx><qty>12</qty></prod>\n\n" +
                        "<prod><name>saw for metal</name><prx>13.80</prx><qty>32</qty></prod>\n\n" +
                        "<prod><name>wood pallet</name><prx>65</prx><qty>21</qty></prod>\n\n" +
                        "<prod><name>circular fan</name><prx>80</prx><qty>8</qty></prod>\n\n" +
                        "<prod><name>exhaust fan</name><prx>62</prx><qty>8</qty></prod>\n\n" +
                        "<prod><name>cattle prod</name><prx>990</prx><qty>2</qty></prod>\n\n" +
                        "<prod><name>window fan</name><prx>62</prx><qty>8</qty></prod>";

        System.out.println(Catalog.catalog(s,"saw"));
//
//        String name = null;
//        String prx = null;
//        String qty = null;
//        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
//        DocumentBuilder builder = null;
//        try{
//            builder = factory.newDocumentBuilder();
//            ByteArrayInputStream bts = new ByteArrayInputStream(s.getBytes("UTF-8"));
//            Document doc = builder.parse(bts);
//            doc.getDocumentElement().normalize();
//            name = doc.getElementsByTagName("name").item(0).getTextContent();
//            prx = doc.getElementsByTagName("prx").item(0).getTextContent();
//            qty = doc.getElementsByTagName("qty").item(0).getTextContent();
//            System.out.println(name + prx + qty);
//        } catch (Exception e){
//            e.printStackTrace();
//        }
    }

    @Test
    public void pass(){
        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
        String s = bCryptPasswordEncoder.encode("123");
        System.out.println(s);
    }

}
